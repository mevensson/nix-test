let
  nixpkgs = import ./nixpkgs.nix;
in
  with import nixpkgs {
    overlays = [
      (import ./overlay.nix)
    ];
  };

  pkgs.mkShell {
    inputsFrom = [
      nix-test
    ];
    buildInputs = [
    ];
  }
