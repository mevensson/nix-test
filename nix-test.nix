{ rustPlatform }:

rustPlatform.buildRustPackage rec {
  pname = "nix-test";
  version = "1.0";
  name = "${pname}-${version}";

  nativeBuildInputs = [
  ];

  buildInputs = [
  ];

  src = ./.;

  cargoSha256 = "0d9lncgljfwq8ij3grbk8r54p051wyvy5jy9vm9srgrc4n2hjz0n";
}
